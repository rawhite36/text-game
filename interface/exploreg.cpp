

#ifndef exploreg_H_H
#define exploreg_H_H


short exp_wait(Your_Guy*, WorldG*);


short exploreg(Your_Guy* dude, WorldG* world)
{
  string buff = world->area[dude->spot[0]][dude->spot[1]];

  buff[0] = toupper(buff[0]);

	cout << "\n\n\n\n"
       << "                                                                            "
       << "****" << buff << " (" << dude->spot[0] << "," << dude->spot[1] << ")****"
       << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
  << "                                                                                                                                                     "
       << "Move, Look:\n\n"
  << "                                                                                                                                                        "
       << "North\n\n"
  << "                                                                                                                                                        "
       << "East\n\n"
  << "             Wait                                                                                                                                       "
       << "South\n\n"
  << "             Menu                                                                                                                                       "
       << "West\n\n\n\n    ";

	getline(cin,buff);

  for(short i=0; i<buff.length(); ++i)
  {
    buff[i] = tolower(buff[i]);

    if(buff[i]==' ')
      buff.erase(i,1);
  }

	for(short i=0; i<50; ++i)
		cout << endl;

  if(buff=="wait")
  {
    if(exp_wait(dude,world)==1)
      return 2;
  }

  if(buff=="menu")
    return 1;

  if(buff.substr(0,4)=="move")
  {
    if(buff.substr(4)=="north")
      dude->move(0);

    if(buff.substr(4)=="east")
      dude->move(1);

    if(buff.substr(4)=="south")
      dude->move(2);

    if(buff.substr(4)=="west")
      dude->move(3);
  }

  if(buff.substr(0,4)=="look")
  {
    if(buff.substr(4)=="north")
      dude->look(0);

    if(buff.substr(4)=="east")
      dude->look(1);

    if(buff.substr(4)=="south")
      dude->look(2);

    if(buff.substr(4)=="west")
      dude->look(3);
  }

  return 0;
}


short exp_wait(Your_Guy* dude, WorldG* world)
{
  string buff = "";

  while(buff!="p" && buff!="proceed")
  {
    cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
         << "                                                         "
         << "You are waiting. Type 'Proceed' or 'P' to continue."
         << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n    ";

    getline(cin,buff);;

	  for(short i=0; i<buff.length(); ++i)
		  buff[i] = tolower(buff[i]);

    for(short i=0; i<50; ++i)
	    cout << endl;
  }

  if(world->mon_life[dude->spot[0]][dude->spot[1]][0].size()>0)
  {
    srand(time(0));

    if(rand()%3==0)
      return 1;
  }

  return 0;
}


#endif
