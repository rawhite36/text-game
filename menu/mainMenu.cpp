//08-14-15


#ifndef mainMenu_cpp
#define mainMenu_cpp


#include "../world/monsterg.cpp"
#include "../world/worldg.cpp"
#include "../char/your_guy.cpp"
#include "proceedg.cpp"


void main_menu(string name, string word)
{

	string buff = "../data/charD/guy" + name + ".txt";

	Your_Guy* dude = new Your_Guy(buff);

  WorldG* world = new WorldG;

	short check = 0;

	do
	{
		cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
         << "                                                                 "
         << "              ****MENU****\n\n\n\n"
         << "                                                                 "
         << "(type first word to navigate)\n\n"
         << "                                                                 "
         << "Proceed                        Inventory\n\n"
         << "                                                                 "
         << "Character Sheet         Books & Journals\n\n"
         << "                                                                 "
         << "Plans                               Maps\n\n"
         << "                                                                 "
         << "World Affairs                Biographies\n\n"
         << "                                                                 "
         << "History               Deeds & Statistics\n\n"
         << "                                                                 "
         << "Options                             Quit"
         << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n    ";

		getline(cin,buff);

		for(short i=0; i<buff.length(); ++i)
			buff[i] = tolower(buff[i]);

		for(short i=0; i<50; ++i)
			cout << endl;

		if(buff=="proceed")
			proceedg(dude,world);

		/*if(buff=="inventory")
			//inventoryg();

		if(buff=="character")
			//characterg();

		if(buff=="books")
			//booksg();

		if(buff=="plans")
			//plansg();

		if(buff=="maps")
			//mapsg();

		if(buff=="world")
			//affairsg();

		if(buff=="biographies")
			//biographiesg();

		if(buff=="history")
			//historyg();

		if(buff=="deeds")
			//deedsg();

		if(buff=="options")
			//saveg();*/

		if(buff=="quit")
		{
			//quitg();
			check = 1;
		}
	}	while(check==0);

  delete world;
  delete dude;
}


#endif
