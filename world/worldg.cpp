//08-13-15


#ifndef worldGen_cpp
#define worldGen_cpp


#define AreaLim 10


#include "inc/worldg.hpp"


string WorldG::area[AreaLim][AreaLim];


WorldG::WorldG()
{
	//srand(time(0));

	for(unsigned int i=0; i<AreaLim; ++i)
	{
		for(unsigned int j=0; j<AreaLim; ++j)
    {
      if((i!=6 || j==3) && i<8)
        area[i][j] = "field";

      if(i==6 && j!=3)
			  area[i][j] = "river";// + rand() % 7;

      if(i>7)
        area[i][j] = "forest";
    }
	}

  mon = new Monster***[AreaLim];
  for(unsigned int i=0; i<AreaLim; ++i)
  {
    mon[i] = new Monster**[AreaLim];

    for(unsigned int j=0; j<AreaLim; ++j)
    {
      mon[i][j] = new Monster*[5];

      for(short k=0; k<5; ++k)
        mon_select(mon[i][j][k],i,j,k);
    }
  }
}



WorldG::~WorldG()
{
  for(unsigned int i=0; i<AreaLim; ++i)
  {
    for(unsigned int j=0; j<AreaLim; ++j)
    {
      for(short k=0; k<5; ++k)
        delete[] mon[i][j][k];

      delete[] mon[i][j];
    }

    delete[] mon[i];
  }

  delete[] mon;
}



void WorldG::mon_select(Monster* mons, unsigned int x, unsigned int y, short z)
{
  mons = new Monster[2];

  mons[0].init(0,0,2);
  mon_life[x][y][z].push_back(0);

	mons[1].init(0,0,1);
  mon_life[x][y][z].push_back(0);
}


#endif
