//08-16-15


#ifndef monsterg_cpp
#define monsterg_cpp


#include "inc/monsterg.hpp"


Monster::Monster()
{}


void Monster::init(short mtype, short mspec, short mlevel)
{
	string mon = "";
	string buff = "";
	char cBuff = ' ';

	mon = (char)(mtype+48);
	mon += (char)(mspec+48);

	ifstream monlist("../data/monD/monList.txt");

	do
	{
		while(cBuff!='&')
			monlist >> cBuff;

		monlist >> cBuff;
		buff += cBuff;
		monlist >> cBuff;
		buff += cBuff;
		monlist >> cBuff;
		monlist >> cBuff;

		if(mon==buff)
		{
			buff = "";
			while(cBuff!='&')
			{
				buff += cBuff;
				monlist >> cBuff;
			}
		}
		else
		{
			buff = "";
			while(cBuff!='&')
				monlist >> cBuff;
			monlist >> cBuff;
		}
	}	while(buff=="");

	monlist.close();

	buff = "../data/monD/" + buff;

	ifstream monFile(buff.c_str());

	buff = "";
	cBuff = ' ';

	do
	{
		while(cBuff!='&')
			monFile >> cBuff;

		monFile >> cBuff;

		if((char)(mlevel+48)>=cBuff && (char)(mlevel+48)<cBuff+10)
		{
			monFile >> cBuff;
			monFile >> cBuff;

			while(cBuff!='&')
				monFile >> cBuff;

			monFile >> cBuff;

			while(cBuff!='&')
			{
				type += cBuff;
				monFile >> cBuff;
			}

			monFile >> cBuff;

			while(cBuff!='&')
				monFile >> cBuff;

			monFile >> cBuff;

			while(cBuff!='&')
			{
				faction += cBuff;
				monFile >> cBuff;
			}

			monFile >> cBuff;

			while(cBuff!='&')
				monFile >> cBuff;

			monFile >> health;			monFile >> cBuff;
			monFile >> spirit;			monFile >> cBuff;
			monFile >> stamina; 		monFile >> cBuff;
			monFile >> strength;		monFile	>> cBuff;
			monFile >> agility;			monFile >> cBuff;
			monFile >> accuracy;		monFile >> cBuff;
			monFile >> perception; 	monFile >> cBuff;
			monFile >> intellect;		monFile	>> cBuff;
			monFile >> creativity; 	monFile >> cBuff;
			monFile >> luck;				monFile	>> cBuff;
			monFile >> cBuff;

			monLevel = ((short)health+(short)spirit+(short)stamina
									+(short)strength+(short)agility+(short)accuracy
									+(short)perception+(short)intellect+(short)creativity
									+(short)luck-480)/10;

			while(cBuff!='&')				//items
				monFile >> cBuff;

			monFile >> cBuff;

			while(cBuff!='&')
			{
				for(short i=0; i<4; i++)
				{
					buff += cBuff;
					monFile >> cBuff;
				}

				items.push_back(buff);
				buff = "";
				if(cBuff!='&')
					monFile >> cBuff;
			}

			monFile >> cBuff;

			while(cBuff!='&')			//weapons
				monFile >> cBuff;

			monFile >> cBuff;

			while(cBuff!='&')
			{
				for(short i=0; i<4; i++)
				{
					buff += cBuff;
					monFile >> cBuff;
				}

				weapons.push_back(buff);
				buff = "";
				if(cBuff!='&')
					monFile >> cBuff;
			}

			monFile >> cBuff;

			while(cBuff!='&')			//armor
				monFile >> cBuff;

			monFile >> cBuff;

			while(cBuff!='&')
			{
				for(short i=0; i<4; i++)
				{
					buff += cBuff;
					monFile >> cBuff;
				}

				armor.push_back(buff);
				buff = "";
				if(cBuff!='&')
					monFile >> cBuff;
			}

			monFile >> cBuff;

			while(cBuff!='&')			//spells
				monFile >> cBuff;

			monFile >> cBuff;

			while(cBuff!='&')
			{
				for(short i=0; i<2; i++)
				{
					buff += cBuff;
					monFile >> cBuff;
				}

				magick.push_back(buff);
				buff = "";
				if(cBuff!='&')
					monFile >> cBuff;
			}

			monFile >> cBuff;

			while(cBuff!='&')			//type specials
				monFile >> cBuff;

			monFile >> cBuff;

			while(cBuff!='&')
			{
				while(cBuff!='/' && cBuff!='&')
				{
					buff += cBuff;
					monFile >> cBuff;
				}

				tspecials.push_back(buff);
				buff = "";
				if(cBuff!='&')
					monFile >> cBuff;
			}

			monFile >> cBuff;

			monFile.close();
		}
		else
		{
			for(short i=0; i<17; i++)
			{
				while(cBuff!='&')
					monFile >> cBuff;

				monFile >> cBuff;
			}
		}
	}	while(monFile.is_open());
}


#endif
