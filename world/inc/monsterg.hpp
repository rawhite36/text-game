

#ifndef monsterg_hpp
#define monsterg_hpp


class Monster
{
	public:
    Monster();

		void init(short, short, short);

		string name;
		string type;
		string faction;

		vector<char> buffSpells;

		char health;
		char spirit;
		char stamina;
		char strength;
		char agility;
		char accuracy;
		char perception;
		char intellect;
		char creativity;
		char luck;

		short monLevel;

		vector<string> items;
		vector<string> weapons;
		vector<string> armor;

		string head;
		string shirt;
		string pants;
		string belt;
		string shoes;
		string gloves;
		string rhand;
		string lhand;
		string quick;

		vector<string> magick;

		vector<string> tspecials;
};


#endif
