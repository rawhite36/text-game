

#ifndef worldg_hpp
#define worldg_hpp


class WorldG
{
  void mon_select(Monster*, unsigned int, unsigned int, short);

  public:
    static string area[AreaLim][AreaLim];

    Monster**** mon;

    vector<short> mon_life[AreaLim][AreaLim][5];

    WorldG();
    ~WorldG();

		//include spot specifics, scenery, objects, monsters, etc.
};


#endif
