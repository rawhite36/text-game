//08-13-15


#ifndef your_guy_H_H
#define your_guy_H_H


#include "inc/your_guy.hpp"


Your_Guy::Your_Guy(string guyFile)
{
	char cBuff = ' ';
	string buff = "";
	
	ifstream guy(guyFile.c_str());

	while(cBuff!='&')			//location
		guy >> cBuff;

	guy >> spot[0];			guy >> cBuff;
	guy >> spot[1];			guy >> cBuff;
	guy >> cBuff;

	while(cBuff!='&')			//name and faction
		guy >> cBuff;

	guy >> cBuff;

	while(cBuff!='/')
	{
		buff += cBuff;
		guy >> cBuff;
	}

	nameF = buff;
	buff = "";
	guy >> cBuff;

	while(cBuff!='/')
	{
		buff += cBuff;
		guy >> cBuff;
	}

	nameL = buff;
	buff = "";
	guy >> cBuff;

	while(cBuff!='&')
	{
		buff += cBuff;
		guy >> cBuff;
	}

	faction = buff;
	buff = "";
	guy >> cBuff;

	while(cBuff!='&')			//buffs
		guy >> cBuff;

	guy >> cBuff;

	while(cBuff!='&')
	{
		buffSpells.push_back(cBuff);
		guy >> cBuff;
		if(cBuff!='&')
			guy >> cBuff;
	}

	guy >> cBuff;

	while(cBuff!='&')			//attributes
		guy >> cBuff;

	buff = "";
	guy >> cBuff;

	while(cBuff!='/')
	{
		buff += cBuff;
		guy >> cBuff;
	}

  health = 0;
  for(short i=0; i<buff.length(); ++i)
    health += pow(10,i)*(buff[buff.length()-i-1]-48);

  buff = "";
  guy >> cBuff;

	while(cBuff!='/')
	{
		buff += cBuff;
		guy >> cBuff;
	}

  spirit = 0;
  for(short i=0; i<buff.length(); ++i)
    spirit += pow(10,i)*(buff[buff.length()-i-1]-48);

  buff = "";
  guy >> cBuff;

	while(cBuff!='/')
	{
		buff += cBuff;
		guy >> cBuff;
	}

  stamina = 0;
  for(short i=0; i<buff.length(); ++i)
    stamina += pow(10,i)*(buff[buff.length()-i-1]-48);

  buff = "";
  guy >> cBuff;

	while(cBuff!='/')
	{
		buff += cBuff;
		guy >> cBuff;
	}

  strength = 0;
  for(short i=0; i<buff.length(); ++i)
    strength += pow(10,i)*(buff[buff.length()-i-1]-48);

  buff = "";
  guy >> cBuff;

	while(cBuff!='/')
	{
		buff += cBuff;
		guy >> cBuff;
	}

  agility = 0;
  for(short i=0; i<buff.length(); ++i)
    agility += pow(10,i)*(buff[buff.length()-i-1]-48);

  buff = "";
  guy >> cBuff;

	while(cBuff!='/')
	{
		buff += cBuff;
		guy >> cBuff;
	}

  accuracy = 0;
  for(short i=0; i<buff.length(); ++i)
    accuracy += pow(10,i)*(buff[buff.length()-i-1]-48);

  buff = "";
  guy >> cBuff;

	while(cBuff!='/')
	{
		buff += cBuff;
		guy >> cBuff;
	}

  perception = 0;
  for(short i=0; i<buff.length(); ++i)
    perception += pow(10,i)*(buff[buff.length()-i-1]-48);

  buff = "";
  guy >> cBuff;

	while(cBuff!='/')
	{
		buff += cBuff;
		guy >> cBuff;
	}

  intellect = 0;
  for(short i=0; i<buff.length(); ++i)
    intellect += pow(10,i)*(buff[buff.length()-i-1]-48);

  buff = "";
  guy >> cBuff;

	while(cBuff!='/')
	{
		buff += cBuff;
		guy >> cBuff;
	}

  creativity = 0;
  for(short i=0; i<buff.length(); ++i)
    creativity += pow(10,i)*(buff[buff.length()-i-1]-48);

  buff = "";
  guy >> cBuff;

	while(cBuff!='&')
	{
		buff += cBuff;
		guy >> cBuff;
	}

  luck = 0;
  for(short i=0; i<buff.length(); ++i)
    luck += pow(10,i)*(buff[buff.length()-i-1]-48);

  buff = "";
	guy >> cBuff;

	charLevel = (health+spirit+stamina+strength+agility+accuracy
							+perception+intellect+creativity+luck)/10;

	while(cBuff!='&')			//items
		guy >> cBuff;

	guy >> cBuff;

	while(cBuff!='&')
	{
		for(short i=0; i<4; i++)
		{
			buff += cBuff;
			guy >> cBuff;
		}

		items.push_back(buff);
		buff = "";
		if(cBuff!='&')
			guy >> cBuff;
	}

	guy >> cBuff;

	while(cBuff!='&')			//weapons
		guy >> cBuff;

	guy >> cBuff;

	while(cBuff!='&')
	{
		for(short i=0; i<4; i++)
		{
			buff += cBuff;
			guy >> cBuff;
		}

		weapons.push_back(buff);
		buff = "";
		if(cBuff!='&')
			guy >> cBuff;
	}

	guy >> cBuff;

	while(cBuff!='&')			//armor
		guy >> cBuff;

	guy >> cBuff;

	while(cBuff!='&')
	{
		for(short i=0; i<4; i++)
		{
			buff += cBuff;
			guy >> cBuff;
		}

		armor.push_back(buff);
		buff = "";
		if(cBuff!='&')
			guy >> cBuff;
	}

	guy >> cBuff;

	while(cBuff!='&')			//spells
		guy >> cBuff;

	guy >> cBuff;

	while(cBuff!='&')
	{
		for(short i=0; i<5; i++)
		{
			buff += cBuff;
			guy >> cBuff;
		}

		magick.push_back(buff);
		buff = "";
		if(cBuff!='&')
			guy >> cBuff;
	}

	guy >> cBuff;

	while(cBuff!='&')			//accomplishments
		guy >> cBuff;

	guy >> cBuff;

	while(cBuff!='&')
	{
		for(short i=0; i<4; i++)
		{
			buff += cBuff;
			guy >> cBuff;
		}

		accomp.push_back(buff);
		buff = "";
		if(cBuff!='&')
			guy >> cBuff;
	}
}


void Your_Guy::move(short dir)
{
  short flag = 0;

  switch(dir)
  {
    case 0:
      if(spot[1]!=AreaLim-1)
        ++spot[1];
      else
        move_bound(0);
      break;

    case 1:
      if(spot[0]!=AreaLim-1)
        ++spot[0];
      else
        move_bound(0);

      flag = 1;
      break;

    case 2:
      if(spot[1]!=0)
        --spot[1];
      else
        move_bound(0);

      flag = 2;
      break;

    case 3:
      if(spot[0]!=0)
        --spot[0];
      else
        move_bound(0);

      flag = 3;
  }

  if(WorldG::area[spot[0]][spot[1]]=="river")
  {
    switch(flag)
    {
      case 0:
        --spot[1];
        break;

      case 1:
        --spot[0];
        break;

      case 2:
        ++spot[1];
        break;

      case 3:
        ++spot[0];
    }

    move_bound(1);
  }
}


void Your_Guy::move_bound(short type)
{
  string buff = "";

  switch(type)
  {
    case 0:
      while(buff!="p" && buff!="proceed")
      {
        cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
             << "                                                "
             << "You have encountered an impenetrable, invisible wall. You are forced\n"
             << "                                                "
             << "to withdraw your attempt to pass. Type 'Proceed' or 'P' to continue."
             << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n    ";

        getline(cin,buff);

        for(short i=0; i<buff.length(); ++i)
          buff[i] = tolower(buff[i]);

        for(short i=0; i<50; ++i)
          cout << endl;
      }

      break;

    case 1:
      while(buff!="p" && buff!="proceed")
      {
        cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
             << "                                               "
             << "You have encountered a river. You are forced to withdraw your attempt\n"
             << "                                                            "
             << "to pass. Type 'Proceed' or 'P' to continue."
             << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n    ";

        getline(cin,buff);

        for(short i=0; i<buff.length(); ++i)
          buff[i] = tolower(buff[i]);

        for(short i=0; i<50; ++i)
          cout << endl;
      }
  }
}


void Your_Guy::look(short dir)
{
  /*switch(dir)
  {
    case 0:

      break;

    case 1:

      break;

    case 2:

      break;

    case 3:

  }*/
}


#endif
