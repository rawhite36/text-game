

#ifndef your_guyH_H_H
#define your_guyH_H_H


class Your_Guy
{
  void move_bound(short);

	public:
		Your_Guy(string);

		void move(short);
		void look(short);
		//void survey();
		//void take();			//object
		//void drop();			//object
		//void interactO(); //object
		//void interactP(); //person

		//location
		unsigned int spot[2];

		//name and faction
		string nameF;
		string nameL;
		string faction;

		//buffs
		vector<char> buffSpells;

		//attributes
		short health;
		short spirit;
		short stamina;
		short strength;
		short agility;
		short accuracy;
		short perception;
		short intellect;
		short creativity;
		short luck;

		//level
		int charLevel;

		//inventory
		vector<string> items;
		vector<string> weapons;
		vector<string> armor;

		//equipped
		string head;
		string shirt;
		string pants;
		string belt;
		string shoes;
		string gloves;
		string rhand;
		string lhand;
		string quick;

		//spells
		vector<string> magick;

		//accomplishments
		vector<string> accomp;

		//health & spirit bars (don't refill after fight)
};


#endif
