//08-12-15


#include <iostream>
#include <fstream>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <vector>

using namespace std;

#include "../start/login.cpp"
#include "../start/intro.cpp"
#include "../start/new_user.cpp"
#include "../menu/mainMenu.cpp"


int main()
{
	string user = "";
	string pass = "";

	for(short i=0; i<100; ++i)
		cout << endl;

	for(short i=0; i<1000; ++i)
	{
		switch(login(user, pass))
		{
			case 0:
				main_menu(user, pass);
				i = 1000;
				break;
			case 1:
				new_user(user, pass);
				intro();
				main_menu(user, pass);
				i = 1000;
				break;
			case 2:
				if(i>5)
					cout << "  Let's Go.." << endl << endl << endl;
				else
					cout << "  Wrong Info" << endl << endl << endl;
		}
	}

	return 0;
}
